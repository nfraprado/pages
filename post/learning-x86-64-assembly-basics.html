<!DOCTYPE html>
<html>
	<head>
			<title>Learning x86-64 assembly basics - nfraprado</title>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
			<link rel="stylesheet" href="https://nfraprado.net/theme/css/main.css" />
			<link rel="stylesheet" href="https://nfraprado.net/theme/css/dark.css" />
				<link href="feeds/all.atom.xml" type="application/atom+xml" rel="alternate" title="nfraprado's Feed" />
	</head>
	<body>
		<nav role="navigation">
			<div>
				<ul>
					<li><a href="https://nfraprado.net/">nfraprado's Blog</a></li>
						<li><a href="https://nfraprado.net/pages/about.html">About</a></li>
					<li><a href="https://nfraprado.net/tags.html">Tags</a></li>
					<li><a href="https://nfraprado.net/feeds/all.atom.xml">Follow</a></li>
							<li>
								<a href="https://nfraprado.net/pt-br/">
											Português (pt-BR)

								</a>
							</li>
				</ul>
			</div>
		</nav>
		<div>
	<section>
		<header>
			<h1>
				Learning x86-64 assembly basics
			</h1>
Translations: 
					<a href="https://nfraprado.net/pt-br/post/aprendendo-o-basico-de-assembly-x86-64.html">
								Português (pt-BR)

					</a>
			<div>Publication date: Feb 25, 2022</div>
			<div>
Tags: 
						<a href="https://nfraprado.net/tag/assembly.html">assembly</a>, 						<a href="https://nfraprado.net/tag/x86-64.html">x86-64</a>			</div>
		</header>
		<hr />
		<div class="e-content">
			<p>Recently I decided to learn assembly. I already had a reasonable understanding
of how it worked due to some classes that touched the subject in university,
however I never had the opportunity to really write assembly code.</p>
<p>Since my everyday computer is an x86-64 machine, it made most sense to learn
assembly for this architecture, so I could avoid the need for a VM. I started
with only the desire to get my hands dirty with assembly code, and not any
particular objective or project.</p>
<p>At first I was alternating between trying things out and researching on the web
just to understand enough to get a bare minimum assembly file and commands that
would assemble it and run. Eventually I stumbled upon the book that would guide
me: <a class="reference external" href="https://open.umn.edu/opentextbooks/textbooks/733">x86-64 Assembly Language Programming with Ubuntu</a>.</p>
<p>This book is free, recent and had the perfect scope for me: it's aimed at people
that already have a good grasp of programming, but are new to x86-64 assembly,
and it shows some theory and concepts, but there are plenty of exercises to
learn from practice.</p>
<p>It was pretty fun to work through that book, and it worked well for me to create
some familiarity with x86-64 assembly. I'm sure there are still a bunch of
things to learn on the subject, since the book only gives a basis, but it was
enough to teach me some interesting things.</p>
<div class="section" id="signedness-and-two-s-complement">
<h2>Signedness and two's complement</h2>
<p>The biggest lesson to me was a better understanding of signedness. I'm used to
seeing <code class="docutils literal">int</code> and <code class="docutils literal">unsigned int</code> in C, and to watch out for using the wrong
signedness, but it wasn't as clear to me how that worked at the assembly level.</p>
<p>The first thing to have in mind, is that the type concept present in higher
level languages like C (like if a number is signed or not) is completely absent
in assembly. The computer memory stores only 0s and 1s, and it's up to you, the
programmer, to interpret what they mean: is <code class="docutils literal">01011000</code> the number 88, the
character <code class="docutils literal">X</code>, the <code class="docutils literal">POP AX</code> instruction? With only that single byte, you
can't even be sure of the size: maybe those are really 8 boolean flags in a
single byte, or part of a 4-byte signed number. Without context it's impossible
to tell.</p>
<p>If the same representation can mean both a signed or unsigned number, depending
on the context, that means that when operating on those numbers, you as the
programmer have to use the right variant of the instruction to give that
context to the computer.</p>
<p>While going through the book, the following arithmetic instructions were
presented for unsigned numbers:</p>
<ul class="simple">
<li><code class="docutils literal">add</code> adds two numbers</li>
<li><code class="docutils literal">sub</code> subtracts two numbers</li>
<li><code class="docutils literal">mul</code> multiplies two numbers</li>
<li><code class="docutils literal">div</code> divides two numbers</li>
</ul>
<p>And the following instructions were shown for comparison between unsigned
numbers:</p>
<ul class="simple">
<li><code class="docutils literal">ja</code> compares two numbers and jumps if the first one is above the second</li>
<li><code class="docutils literal">jb</code> compares two numbers and jumps if the first one is below the second</li>
</ul>
<p>And sure enough, shortly after, the signed variants of those instructions were
also shown:</p>
<ul class="simple">
<li><code class="docutils literal">imul</code> is <code class="docutils literal">mul</code>'s signed variant</li>
<li><code class="docutils literal">idiv</code> is <code class="docutils literal">div</code>'s signed variant</li>
<li><code class="docutils literal">jg</code> is <code class="docutils literal">ja</code>'s signed variant</li>
<li><code class="docutils literal">jl</code> is <code class="docutils literal">jb</code>'s signed variant</li>
</ul>
<p>But wait, what about <code class="docutils literal">iadd</code> and <code class="docutils literal">isub</code>? That's the thing, the way x86-64
represents negative numbers is through the use of the <a class="reference external" href="https://en.wikipedia.org/wiki/Two%27s_complement">two's complement</a>
system, which has the useful property of allowing addition and subtraction to be
done in the exactly same manner for both signed and unsigned values.</p>
<p>This means that there's only one way to add, independently of the signedness,
and it's using <code class="docutils literal">add</code>. There's no <code class="docutils literal">iadd</code>. Likewise for subtraction.</p>
<p>So the interesting conclusion is that for addition and subtraction it doesn't
matter if you use <code class="docutils literal">unsigned int</code> or <code class="docutils literal">int</code> for the variables in C. The
<code class="docutils literal">unsigned</code> keyword is there for you to tell the compiler to use the right
variant of the instruction in the generated assembly, which is required when
you're comparing numbers (<code class="docutils literal">ja</code> vs <code class="docutils literal">jg</code>, <code class="docutils literal">jb</code> vs <code class="docutils literal">jl</code>), multiplying
(<code class="docutils literal">mul</code> vs <code class="docutils literal">imul</code>) or dividing (<code class="docutils literal">div</code> vs <code class="docutils literal">idiv</code>). But thanks to two's
complement, in addition and subtraction there's no way to get it wrong 🙂.</p>
<p>Side note: interestingly, while writing this post, I read on the Wikipedia page
that two's complement also works the same for multiplication, but only if you do
a sign extend of the two operands beforehand. Which makes me think that if the
<code class="docutils literal">mul</code> instruction always did the sign extend step, no <code class="docutils literal">imul</code> instruction
would be required as well, but that would probably increase complexity (and
cost) in the logic circuitry.</p>
</div>
<div class="section" id="other-interesting-lessons">
<h2>Other interesting lessons</h2>
<p>The other thing that interested me the most was to realize that local variables
are nothing more than adding more space to the stack. And that this is done
simply by subtracting the stack register <code class="docutils literal">rsp</code> by the total number of bytes
needed for the variables at the start of a subroutine.</p>
<p>Also interesting was to learn how there are <a class="reference external" href="https://en.wikipedia.org/wiki/X86_calling_conventions#x86-64_calling_conventions">calling conventions</a> to
standardize on:</p>
<ul class="simple">
<li>which registers are used to pass arguments to subroutines and in which order;</li>
<li>which registers can be overwritten by a subroutine and which should be left
unchanged. When using the latter, its current value should first be pushed on
the stack so that it can be restored before returning.</li>
</ul>
<p>And what about the magic <code class="docutils literal">main()</code> function that the C compiler expects in
every C program? Assembly doesn't need compiling, so no need for that, but turns
out a different magic label is expected by the linker: <code class="docutils literal">_start</code>.</p>
<p>Some other things that were interesting to do in assembly:</p>
<ul class="simple">
<li>Making syscalls</li>
<li>Exploiting a stack buffer overflow</li>
<li>Interacting assembly code with C code, and vice versa.</li>
</ul>
</div>
<div class="section" id="lack-of-a-good-gui">
<h2>Lack of a good GUI</h2>
<p>One thing I missed was a good GUI application when debugging the assembly
programs. It would have been really helpful to have one that showed the values
of expressions in tooltips when hovering, that was able to follow labels when
clicking, and so on.</p>
<p>The book recommends using DDD, which is a GUI, but it felt clunky and really
outdated. I went for using GDB together with the <a class="reference external" href="https://github.com/longld/peda">peda</a> plugin, and that worked
reasonably well, but being a CLI, every inspection required divining the correct
command, so it took more time to get oriented.</p>
</div>
<div class="section" id="conclusion">
<h2>Conclusion</h2>
<p>This was a great experience and I hope to get back to it and further my
knowledge past the &quot;basic&quot; level for x86-64 sometime in the future. Seeing
what's happening at the assembly level really helps better understand the higher
level languages, and value the way they hide complexities below!</p>
<p>I've uploaded the code I wrote for all the book's exercises <a class="reference external" href="https://codeberg.org/nfraprado/x86-64-book-exercises">to this
repository</a>. I don't expect it to be useful to anyone since it's simple stuff,
but it's there either way.</p>
<p>The only exercise that I couldn't actually finish was the last one. There's very
little information on the book about how to do it, and during research of the
topic online I eventually got demotivated and started learning about other
subjects instead. Maybe one day I'll give it another try. If you do know how to
do it, <a class="reference external" href="/pages/about.html">get in touch</a>! 🙂</p>
<p>And even though I couldn't finish that last exercise, while researching about it
I ended up learning about how to use the <code class="docutils literal">asm</code> syntax for GCC <a class="reference external" href="https://www.felixcloutier.com/documents/gcc-asm.html">through this
guide</a>, to embed assembly in a C file, and also about the <a class="reference external" href="https://godbolt.org/">Compiler
Explorer</a> which seems a great way to learn about assembly and C by just seeing
what assembly is generated from a given C code, so I'm calling this a win!</p>
</div>

		</div>
	</section>
		</div>
		<hr />
		<footer>
			<div>
				<p>&copy; 2020-2024 Nícolas F. R. A. Prado</p>
				<p>
Content licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a>.
					<a href="https://codeberg.org/nfraprado/blog">Code</a> licensed under <a href="https://www.gnu.org/licenses/agpl-3.0.html">AGPLv3</a>
				</p>
				<p>
Powered by <a href="http://getpelican.com/">pelican</a>.
Theme by <a href="https://seirdy.one">seirdy</a>
				</p>
			</div>
		</footer>
	</body>
</html>